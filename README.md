# Wie man eine Aufgabe öffentlich vergibt?

Diese Projekt ist ein Beispiel für eine öffentliche Vergabe von Coding. Löse die gestellte Aufgabe und kassiere die ausgeschriebene Summe.

## Die Aufgabe
Die Aufgabe ist im Pythonfile: [./aufgabe.py](aufgabe.py) beschrieben.
Es geht um die Implementierung der hier beschriebenen Interfaces.

## Die Belohnung
Ist bisher 1€.

## Die Bewertung
Die Bewertung der Aufgabe erfolgt automatisiert durch die hierin verbauten Unittests im [Verzeichnis tests](./tests/) welche beim Checkin ausgeführt werden.
Wer das die Test als erstes erfolgreich durchläuft, gewinnt die Belohnung.

## Weitere Informationen 

Gibt es hier: <a href="https://gitlab.com/marcolinjo/my-private-service">https://gitlab.com/marcolinjo/my-private-service</a>.
Einen Ansprechpartner für dieses Repo ist [Marco Apel](https://www.facebook.com/flupenjo). 